FROM jupyter/tensorflow-notebook:04f7f60d34a6

USER root

RUN apt-get update && \
	apt-get install -y --no-install-recommends libspatialindex-dev=1.9.3-1build1 && \
	rm -rf /var/lib/apt/lists/*

USER $NB_USER

RUN pip install 'geopandas==0.7' \
				'psycopg2-binary==2.8.5' \
				'pysal==2.2.0' \
				'xlwt==1.3.0' \
				'pysmb==1.2.1' \
				'osmnx==0.14.1' \
				'momepy==0.2.1' \
				'squarify==0.4.3' \
				'jupyter_contrib_nbextensions==0.5.1' \
				'papermill==0.10.1' \
				'ipyleaflet==0.13.3' \
				'keplergl==0.2.1' && \
	jupyter contrib nbextension install --user && \
	fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"
